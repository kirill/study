require 'minitest/autorun'

require 'minitest/reporters'
reporter_options = { color: true }
Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new(reporter_options)]

require './degrees_convertor.rb'

class DegreesConvertorTest < Minitest::Test
  def test_convert_negative_value
    assert_equal -1768, ::DegreesConvertor.convert_to_fahrenheit(-1000)
  end

  def test_convert_zero
    assert_equal 32, ::DegreesConvertor.convert_to_fahrenheit(0)
  end

  def test_convert_positive_value
    assert_equal 1832, ::DegreesConvertor.convert_to_fahrenheit(1000)
  end

  def test_convert_invalide_data_type
    exception = assert_raises RuntimeError do
      ::DegreesConvertor.convert_to_fahrenheit('some text')
    end

    assert_equal("вы ввели не число", exception.message)
  end
end
