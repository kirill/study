class DegreesConvertor
  def self.convert_to_fahrenheit(degrees)
    fail "вы ввели не число" unless degrees.is_a?(Numeric)
    (degrees * 9 / 5) + 32
  end
end
